##alslib.js Javascript Library

alslib.js is a micro library used for manipulating the [DOM](http://en.wikipedia.org/wiki/Document_Object_Model) (Document Object Module).

* license: MIT - http://opensource.org/licenses/MIT
* author: Alex Oliver
* year: 2014 - 2016
* Repo: https://bitbucket.org/aoliver/alslib.js

###Key Features

* Easy to use
* Small in size, 4kb
* Easily extendable

###Browser Support

alslib.js has been developed to work across major browser releases on both desktop and mobile platforms.

* Google Chrome
* Mozilla Firefox
* Apple Safari
* Opera
* Microsoft Internet Explorer 10, 11
* Microsoft Edge

###Quick Usage

```javascript
(function () {
	if (window.alslib) {

		var alslib = window.alslib;

		alslib('a').bind({
			click: function (e) {
				e.preventDefault();
				console.log('test');
			}
		});
	}
}());
```

###Avaliable Functions

* each(function)
* eq(int)
* id(int)
* index(int, boolean)
* lastNode()
* find(string)
* seek(object, dom node)
* parent(string)
* parents(string)
* html(string)
* append(string)
* prepend(string)
* empty()
* remove()
* text(string)
* style(object or string)
* hasClass(string)
* addClass(string)
* removeClass(string)
* toggleClass(string)
* attr(object or string)
* removeAttr(string)
* bind(object or string, function)
* unbind(string, function)
* on(string, function)
* delegate(string, string, function)