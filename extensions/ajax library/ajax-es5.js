/*
    A simple ajax library

    @license: MIT - http://opensource.org/licenses/MIT
    @version: 1.0.0 Development
    @author: Alex Oliver
    @Repo: https://bitbucket.org/aoliver/alslib.js

    Copyright (c) 2014 - 2016 Alex Oliver
*/
/* jslint params */

/*jslint
    browser
*/

/*global
    window
*/
(function () {

    'use strict';

    window.ajax = function (theParams) {

        var defaults = {
            url: '',
            type: 'GET',
            dataType: 'text',
            data: null,
            contentType: 'application/x-www-form-urlencoded',
            progress: null,
            error: null,
            complete: null,
            canceled: null
        };

        if (typeof theParams === 'object') {
            Object.keys(theParams).forEach(function (thisOption) {
                defaults[thisOption] = theParams[thisOption];
            });
        }

        var sendData = "";

        var xmlhttp = new XMLHttpRequest();

        //data
        if (typeof defaults.data === 'object') {
            Object.keys(defaults.data).forEach(function (thisDataRow) {
                if (sendData !== '') {
                    sendData += '&';
                }
                sendData += thisDataRow + '=' + defaults.data[thisDataRow];
            });
        } else if (typeof defaults.data === 'string') {
            sendData = defaults.data;
        }

        //progress
        xmlhttp.addEventListener('progress', defaults.progress);
        xmlhttp.addEventListener('load', defaults.complete);
        xmlhttp.addEventListener('error', defaults.error);
        xmlhttp.addEventListener('abort', defaults.canceled);

        //request
        xmlhttp.open(defaults.type, defaults.url);
        xmlhttp.setRequestHeader("Content-type", defaults.contentType);
        xmlhttp.send(sendData);

        return xmlhttp;
    };

}());