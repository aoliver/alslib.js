/*
    A simple ajax library

    @license: MIT - http://opensource.org/licenses/MIT
    @version: 1.0.0 Development
    @author: Alex Oliver
    @Repo: https://bitbucket.org/aoliver/alslib.js

    Copyright (c) 2014 - 2016 Alex Oliver
*/
(function(){window.ajax=function(d){var b={url:"",type:"GET",dataType:"text",data:null,contentType:"application/x-www-form-urlencoded",progress:null,error:null,complete:null,canceled:null};"object"===typeof d&&Object.keys(d).forEach(function(a){b[a]=d[a]});var c="",a=new XMLHttpRequest;"object"===typeof b.data?Object.keys(b.data).forEach(function(a){""!==c&&(c+="&");c+=a+"="+b.data[a]}):"string"===typeof b.data&&(c=b.data);a.addEventListener("progress",b.progress);a.addEventListener("load",b.complete);
a.addEventListener("error",b.error);a.addEventListener("abort",b.canceled);a.open(b.type,b.url);a.setRequestHeader("Content-type",b.contentType);a.send(c);return a}})();