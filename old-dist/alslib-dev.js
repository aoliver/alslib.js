/*
    alslib.js Javascript Library

    @license: MIT - http://opensource.org/licenses/MIT
    @version: 1.0.7 Development
    @author: Alex Oliver
    @Repo: https://bitbucket.org/aoliver/alslib.js

    11/08/2015: Added toggleClass function
    11/08/2015: Added id function
    11/08/2015: Fixed issue with bind core object origin
    12/08/2015: Update bind function to allow string and callback function to be passed .bind('click',function(){ do somthing })
    12/08/2015: Fixed issue with delegate function not registering origin object
    15/08/2015: Added 'on' function .on('click',function(){});
    16/10/2015: Added ability to return object when calling the append and prepend functions
    16/10/2015: Added objectLength function to alsLibCore - This function will return a length (int) val of a parsed object
    16/10/2015: Added _callback and _timeout params to toggleClass function
    05/01/2016: Removed semicolon from start of self executing function
    05/01/2016: attr function now returns the lib instance when removing a node attribute, this fixes a chaining issue
    05/01/2016: Converted tab indents to spaces
    05/01/2016: Added function returnBindEvent to alsLibCore. This is for use with the delegate function
    05/01/2016: The delegate function now has the ability to process multiple event handlers 

    Copyright (c) 2014 - 2016 Alex Oliver
 */
(function(){

    'use strict';

    var alslib = function( _selector, _node ){
        return new alsLibCore.init( _selector, _node );
    },
    alsLibCore = {
        init: function( _selector, _node ){
            var _return = {},
            _elms = [],
            _obs = [];
            if( document.querySelectorAll ){
                switch( typeof _selector ){
                    case 'string':
                        if( typeof _node === 'object' && _node.length > 0 ){
                            if( _node.length > 1 ){
                                for( var _nodeIndex in _node ){
                                    if( _node[ _nodeIndex ] ){
                                        var _nodeSelector = _node[ _nodeIndex ].querySelectorAll( _selector );
                                        for( var _thisNodeIndex in _nodeSelector ){
                                            if( _thisNodeIndex ){ _obs.push( _nodeSelector[ _thisNodeIndex] ); }
                                        }
                                    }
                                }
                            }
                            else{ _obs = _node[0].querySelectorAll( _selector ); }
                        }
                        else{ _obs = document.querySelectorAll( _selector ); }
                        for( var _index in _obs ){
                            if( _index && _obs[ _index ].nodeType === 1 ){ _elms.push( _obs[ _index ] ); }
                        }
                    break;

                    case 'object':
                        if( _selector === window || _selector === document || _selector.nodeType === 1 ){ _elms.push( _selector ); }
                    break;
                }

                _return.objs = _elms;
                for( var _func in alsLibCore.fn ){ if( alsLibCore.fn[ _func ] ){ _return[ _func ] = alsLibCore.fn[ _func ]; } }
                _return.length = _elms.length;
            }
            else{ _return = false; }
            return _return;
        },

        fn: {
            each: function( _callback ){
                for( var _index in this.objs ){
                    if( _index ){
                        if( typeof _index === 'string' && _index === '0' ){ _index = 0; }
                        else{ _index = parseInt( _index, 10 ); }
                        _callback.call( this.objs[ _index ], this.objs[ _index ], _index );
                    }
                }
            },
            eq: function( _index ){
                if( typeof _index === 'number' && this.objs[ _index ] ){
                    return new alsLibCore.init( this.objs[ _index ] );
                }
            },
            id: function( _index ){
                if( typeof _index === 'number' && this.objs[ _index ] ){
                    return this.objs[ _index ];
                }
            },
            index: function( _elm, _returnObject ){
                for( var _index in this.objs ){
                    if( this.objs[ _index ] === _elm ){ 
                        if( _returnObject ){ return new alsLibCore.init( _elm ); }
                        else{ return parseInt( _index, 10 ); }
                    }
                }
            },
            find: function( _selector ){
                return new alsLibCore.init( _selector, this.objs );
            },
            seek: function( _context, _seek ){
                var _return = false;
                _context.each(function(){
                    var _thisContext = alslib(this);
                    _thisContext.find( _seek.nodeName.toLowerCase() ).each(function(){
                        if( this === _seek ){ _return = _thisContext; }
                    });
                });
                return _return;
            },
            parents: function( _selector, _returnThisParent, _context ){
                var _returnParents = this.parent();
                var _found = _returnParents.find( _selector );
                if( _returnThisParent && !_context ){ _context = this.objs[0]; }
                if( _found.objs.length <= 0 ){ return _returnParents.parents( _selector, _returnThisParent, _context ); }

                else{
                    if( _returnThisParent && typeof _context === 'object' ){ return this.seek( _found, _context ); }
                    else{ return _found; }
                }
            },
            parent: function(){
                return new alsLibCore.init( this.objs[0].parentNode );
            },
            html: function( _html ){
                var _return = this;
                this.each(function( e ){
                    if( typeof _html === 'string' ){ e.innerHTML = _html; }
                    else{ _return = e.innerHTML; }
                });
                return _return;
            },
            append: function( _html, _returnObj ){
                var _return = this;             
                this.each(function( e ){
                    if( typeof _html === 'string' ){
                        var appendedObj = alsLibCore.procesHTML( e, _html, 'append' )[0];
                        if( _returnObj ){ _return = new alsLibCore.init( appendedObj ); }
                    }
                });
                return _return;
            },
            prepend: function( _html, _returnObj ){
                var _return = this;             
                this.each(function( e ){
                    if( typeof _html === 'string' ){
                        var appendedObj = alsLibCore.procesHTML( e, _html, 'prepend' )[0];
                        if( _returnObj ){ _return = new alsLibCore.init( appendedObj ); }
                    }
                });
                return _return;
            },
            empty: function(){
                this.each(function( e ){ e.innerHTML = ""; });
                return this;
            },
            remove: function(){
                this.each(function( e ){ this.parentNode.removeChild( e ); });
            },
            text: function( _string ){
                var _return = this;
                this.each(function( e ){
                    if( typeof _string === 'string' ){ if( !e.textContent ){ e.innerText = _string; } else{ e.textContent = _string; } }
                    else{ if( !e.textContent ){ _return = e.innerText; } else{ _return = e.textContent; } }
                });
                return _return;
            },
            value: function( _value ){
                var _return = this;
                this.each(function( e ){
                    if( typeof _value === 'string' ){ e.value = _value; }
                    else{ _return = e.value; }
                });
                return _return;
            },
            style: function( _styles ){
                var _return = this;
                this.each(function( e ){
                    switch( typeof _styles ){
                        case 'object':
                            for( var _index in _styles ){ if( _index ){ e.style[ _index ] = _styles[ _index ]; } }
                        break;
                        case 'string':
                            if( e.style[ _styles ] ){ _return =  e.style[ _styles ]; }
                        break;
                    }
                });
                return _return;
            },
            hasClass: function( _className, _add, _remove ){
                if( typeof _className === 'string' ){
                    var _findClass = new RegExp( _className ),
                    _return = this;
                    
                    this.each(function( e ){
                        if( _findClass.test( e.className ) ){ 
                            if( _remove ){
                                e.className = e.className.replace( _className, '' ).replace( '  ',' ' );
                            }
                        }
                        else{ 
                            _return = false; 
                            if( _add ){ e.className += ' '+_className; }
                        }
                    });
                    
                    if( _add || _remove ){ _return = this; }
                    return _return;
                }
            },
            addClass: function( _className ){
                return this.hasClass( _className, true );
            },
            removeClass: function( _className ){
                return this.hasClass( _className, false, true );
            },
            toggleClass: function( className, _callback, _timeout ){
                if( typeof className === 'string' ){
                    var _this = this;
                    var hasClass = _this.hasClass( className );
                    if( hasClass ){ _this.removeClass( className ); }
                    else{ _this.addClass( className ); }

                    if( typeof _callback === 'function' ){
                        if( typeof _timeout === 'number' ){ setTimeout(function(){ _callback.call(_this,_this); },_timeout); }
                        else{ _callback.call(_this,_this); }
                    }
                }
            },
            attr: function( _attrName, _remove ){
                var _return = this,
                _type = typeof _attrName;
                this.each(function( e ){
                    switch( _type ){
                        case 'string':
                            if( _remove ){ e.removeAttribute( _attrName ); }
                            else if( e.attributes[ _attrName ] ){ _return = e.attributes[ _attrName ].value; }
                            else{ _return = false; }
                        break;
                        case 'object':
                            for( var _thsAttr in _attrName ){ if( _thsAttr ){ e.setAttribute( _thsAttr, _attrName[ _thsAttr ] ); } }
                        break;
                    }
                });
                return _return;
            },
            removeAttr: function( _attrName ){
                return this.attr( _attrName, true );
            },
            position: function(){
                var _position = { height: 0, width: 0, left: 0, top: 0 };
                this.each(function(){
                    _position.height = this.offsetHeight;
                    _position.width = this.offsetWidth;
                    _position.left = this.offsetLeft;
                    _position.top = this.offsetTop;
                });
                return _position;
            },
            bind: function( _events, _callback ){
                this.each(function( e ){
                    if( typeof _events === 'object' ){
                        for( var _index in _events ){
                            if( typeof _events[ _index ] === 'function' ){ alsLibCore.bindCore( e, _index, _events[ _index ] ); }
                        }
                    }
                    else if( typeof _events === 'string' && typeof _callback === 'function' ){
                        alsLibCore.bindCore( e, _events, _callback );
                    }
                });
                return this;
            },
            on: function( _event, _callback ){
                return this.bind( _event, _callback );
            },
            delegate: function( _selector, _event, _callback ){
                var _this = this,
                _bind = {};

                switch( typeof _event ){
                    case 'object':
                        for( var theEvent in _event ){
                            if( theEvent && typeof _event[ theEvent ] === 'function' ){
                                _bind[ theEvent ] = alsLibCore.returnBindEvent( _this, _event[ theEvent ], _selector  );
                            }
                        }
                    break;

                    case 'string':
                        _bind[ _event ] = alsLibCore.returnBindEvent( _this, _callback, _selector  );
                    break;
                }

                _this.bind( _bind );
            }
        },

        returnBindEvent: function( _this, _callback, _selector ){
            return function( e ){
                var _thisObj = e.target;
                if( typeof document.addEventListener !== 'function' ){ _thisObj = window.event.srcElement; }
                _this.find( _selector ).each(function( _node ){
                    if( _node === _thisObj ){_callback.call( _thisObj, e );}
                    else if( _node === _thisObj.parentNode ){ _callback.call( _thisObj.parentNode, e ); }
                });
            };
        },

        procesHTML: function( _obj, _html, _type ){
            if( typeof _html === 'string' ){
                var _newElm = document.createElement( _obj.nodeName ),
                _this = this,
                _return = {};

                _newElm.innerHTML = _html;
                var _nodeChildren = _newElm.childNodes,
                _firstChild = _obj.firstChild;
                for( var _child in _nodeChildren ){
                    if( _nodeChildren[ _child ].nodeType === 1  ){
                        var _theClone = _nodeChildren[ _child ].cloneNode( true );
                        switch( _type ){
                            case 'append': _obj.appendChild( _theClone ); _return[ _this.objectLength( _return ) ] = _theClone; break;
                            case 'prepend': _obj.insertBefore( _theClone, _firstChild ); _return[ _this.objectLength( _return ) ] = _theClone; break;
                        }
                    }
                }

                return _return;
            }
        },

        bindCore: function( _obj, _event, _callback ){
            if( typeof _obj.addEventListener === 'function' ){ _obj.addEventListener( _event, function( e ){
                if( typeof e.preventDefault !== 'function' ){ e.preventDefault = function( e ){ e.returnValue = false; }; }
                e.origin = _obj;
                _callback.call( e.origin, e );
            }, false ); }
            
            else{ _obj.attachEvent( 'on'+_event, function( e ){
                if( typeof e.preventDefault !== 'function' ){ e.preventDefault = function(){ window.event.returnValue = false; }; }
                e.origin = _obj;
                _callback.call( window.event.srcElement, e );
            }); }
        },

        returnDefaults: function( _defaults, _settings ){
            if( typeof _defaults === 'object' && typeof _settings === 'object' ){
                for( var _setting in _settings ){
                    if( _setting ){ _defaults[ _setting ] = _settings[ _setting]; }
                }   
            }
            return _defaults;
        },

        objectLength: function( _object ){
            var theLength = 0;
            if( typeof _object === 'object' ){ for( var index in _object ){ if( index ){ theLength += 1; }}}
            return theLength;
        }
    };

    window.alslib = alslib;
    window.alsLibCore = alsLibCore;

}());