/*
    alslib-es5.js Javascript Library

    @license: MIT - http://opensource.org/licenses/MIT
    @version: 2.0.4 Development
    @author: Alex Oliver
    @Repo: https://bitbucket.org/aoliver/alslib.js

    Copyright (c) 2014 - 2016 Alex Oliver

    18/01/2016 Added lastNode function
    10/03/2016 Amend addClass, removeClass and toggleClass to return lib object if false
    23/03/2016 Parent function now accepts selector param
    23/03/2016 removed returnThisParent param, use parent function instead
    03/04/2016 removed some invalid case breaks as per jslint validation
    03/04/2016 renamed alsLibCore.init function to alsLibCore.Init as per jslint validation
    17/04/2016 Amend: Spelling fail in index function
    18/04/2016 Added: hasNodes property to core object
    17/05/2016 Fixed: issue with Init function not cycling through each node when using the find function.
    17/05/2016 Added: alsLibFunctions is now global enabling the library core functions class to be extended
    12/06/2016 Added: unbind(theEvents, theEvent) function. Unbind event listeners.
    12/06/2016 Amended: bind function will now accept a comma delimited string.
    05/11/2016 Replaced: ' with "
    05/11/2016 Amended: alslib will return false if no nodes are found.

    09/11/2016 Remove: alslib will return false if no nodes are found. This has been removed because in practice it's really annoying.
    09/11/2016 Amended: toggleClass will return true of false if a class has been added(true) or removed(false). This method used to return the alslib library.
*/

/* jslint params */

/*jslint
    this, browser
*/

/*global
    window
*/

(function (alsLibFunctions, alsLibCore, alslib) {

    "use strict";

    alsLibFunctions = {
        each: function (theCallback) {
            this.nodes.forEach(function (thisNode, theIndex) {
                theCallback.call(thisNode, thisNode, theIndex);
            });
        },
        eq: function (index) {
            if (typeof index === "number" && this.nodes[index]) {
                return new alsLibCore.Init(this.nodes[index]);
            }
        },
        id: function (index) {
            if (typeof index === "number" && this.nodes[index]) {
                return this.nodes[index];
            }
        },
        index: function (theNode, returnLibObject) {
            var theReturn = null;
            this.nodes.forEach(function (thisNode, theIndex) {
                if (thisNode.isEqualNode(theNode)) {
                    theReturn = returnLibObject
                        ? new alsLibCore.Init(thisNode)
                        : theIndex;
                }
            });
            return theReturn;
        },
        lastNode: function () {
            return this.nodes[this.nodes.length - 1];
        },
        find: function (theSelector) {
            return new alsLibCore.Init(theSelector, this.nodes);
        },
        seek: function (context, seek) {
            var theReturn = false;
            context.each(function () {
                if (!theReturn) {
                    var thisContext = alslib(this);
                    thisContext.find(seek.nodeName.toLowerCase()).each(function () {
                        if (this === seek) {
                            theReturn = thisContext;
                        }
                    });
                }
            });
            return theReturn;
        },
        parent: function (selector) {
            if (this.nodes.length > 0) {
                return selector
                    ? this.seek(new alsLibCore.Init(selector), this.nodes[0])
                    : new alsLibCore.Init(this.nodes[0].parentNode);
            }
        },
        parents: function (selector) {
            if (this.nodes.length > 0) {
                var thisParent = this.parent();
                var seeked = thisParent.find(selector);
                return seeked.length > 0
                    ? seeked
                    : thisParent.parents(selector);
            }
        },
        html: function (theHtml, theType) {
            var theReturn = this;
            this.each(function (e) {
                if (theHtml && typeof theHtml === "string") {
                    switch (theType) {
                    case "append":
                        e.insertAdjacentHTML("beforeend", theHtml);
                        break;
                    case "prepend":
                        e.insertAdjacentHTML("afterbegin", theHtml);
                        break;
                    default:
                        e.innerHTML = theHtml;
                    }
                } else {
                    theReturn = e.innerHTML;
                }
            });
            return theReturn;
        },
        append: function (theHtml) {
            this.html(theHtml, "append");
            return this;
        },
        prepend: function (theHtml) {
            this.html(theHtml, "prepend");
            return this;
        },
        empty: function () {
            this.each(function (e) {
                e.innerHTML = "";
            });
            return this;
        },
        remove: function () {
            this.each(function (e) {
                this.parentNode.removeChild(e);
            });
        },
        text: function (theText) {
            var theReturn = this;
            this.each(function (e) {
                if (typeof theText === "string") {
                    e.textContent = theText;
                } else {
                    theReturn = e.textContent;
                }
            });
            return theReturn;
        },
        style: function (theStyles) {
            var theReturn = this;
            this.each(function (e) {
                switch (typeof theStyles) {
                case "object":
                    Object.keys(theStyles).forEach(function (thisStyle) {
                        e.style[thisStyle] = theStyles[thisStyle];
                    });
                    break;
                case "string":
                    if (e.style[theStyles]) {
                        theReturn = e.style[theStyles];
                    }
                    break;
                }
            });
            return theReturn;
        },
        hasClass: function (theClassName, theType) {
            var classRegex = new RegExp(theClassName);
            var theReturn = false;
            var thisLib = this;
            this.each(function (e) {
                if (typeof theClassName === "string") {
                    switch (theType) {
                    case "add":
                        e.classList.add(theClassName);
                        theReturn = thisLib;
                        break;
                    case "remove":
                        e.classList.remove(theClassName);
                        theReturn = thisLib;
                        break;
                    case "toggle":
                        theReturn = e.classList.toggle(theClassName);
                        break;
                    default:
                        if (classRegex.test(e.className)) {
                            theReturn = thisLib;
                        }
                    }
                }
            });
            return theReturn;
        },
        addClass: function (theClassName) {
            return this.hasClass(theClassName, "add") || this;
        },
        removeClass: function (theClassName) {
            return this.hasClass(theClassName, "remove") || this;
        },
        toggleClass: function (theClassName) {
            return this.hasClass(theClassName, "toggle");
        },
        attr: function (attrName, remove) {
            var theReturn = this;
            this.each(function (e) {
                switch (typeof attrName) {
                case "string":
                    if (remove) {
                        e.removeAttribute(attrName);
                    } else if (e.hasAttribute(attrName)) {
                        theReturn = e.getAttribute(attrName);
                    } else {
                        theReturn = false;
                    }
                    break;
                case "object":
                    Object.keys(attrName).forEach(function (thisAttr) {
                        e.setAttribute(thisAttr, attrName[thisAttr]);
                    });
                    break;
                }
            });
            return theReturn;
        },
        removeAttr: function (attrName) {
            return this.attr(attrName, true);
        },
        bind: function (theEvents, theCallback) {
            var eventsArray = theEvents;
            this.each(function (e) {
                switch (typeof theEvents) {
                case "object":
                    Object.keys(theEvents).forEach(function (thisEvent) {
                        if (typeof theEvents[thisEvent] === "function") {
                            e.addEventListener(thisEvent, theEvents[thisEvent]);
                        }
                    });
                    break;
                case "string":
                    eventsArray = theEvents.split(",");
                    eventsArray.forEach(function (thisEvent) {
                        e.addEventListener(thisEvent.trim(), theCallback);
                    });
                    break;
                }
            });
            return this;
        },
        unbind: function (theEvents, theEvent) {
            theEvents = theEvents.split(",");
            this.each(function (e) {
                theEvents.forEach(function (thisEvent) {
                    e.removeEventListener(thisEvent.trim(), theEvent, false);
                });
            });
            return this;
        },
        on: function (theEvent, theCallback) {
            return this.bind(theEvent, theCallback);
        },
        delegate: function (theSelector, theEvent, theCallback) {
            var thisLib = this;
            var theBinds = {};
            switch (typeof theEvent) {
            case "object":
                Object.keys(theEvent).forEach(function (thisEvent) {
                    if (typeof theEvent[thisEvent] === "function") {
                        theBinds[thisEvent] = alsLibCore.returnBindEvent(thisLib, theEvent[thisEvent], theSelector);
                    }
                });
                break;
            case "string":
                theBinds[theEvent] = alsLibCore.returnBindEvent(thisLib, theCallback, theSelector);
                break;
            }

            this.bind(theBinds);
        }
    };

    alsLibCore = {
        Init: function (theSelector, theNode) {
            var theReturn = {};
            var theElms = [];
            var theNodes = [];
            if (document.querySelectorAll) {
                switch (typeof theSelector) {
                case "string":
                    if (typeof theNode === "object") {
                        if (theNode.length && theNode.length > 1) {
                            theNode.forEach(function (nodeIndex) {
                                var nodeSelector = nodeIndex.querySelectorAll(theSelector);
                                [].forEach.call(nodeSelector, function (thisNode) {
                                    theNodes.push(thisNode);
                                });
                            });
                        } else {
                            if (theNode.length > 0) {
                                theNodes = theNode[0].querySelectorAll(theSelector);
                            }
                        }
                    } else {
                        theNodes = document.querySelectorAll(theSelector);
                    }

                    [].forEach.call(theNodes, function (thisNode) {
                        theElms.push(thisNode);
                    });

                    break;
                case "object":
                    if (theSelector && (theSelector === window || theSelector === document || (theSelector.nodeType && theSelector.nodeType === 1))) {
                        theElms.push(theSelector);
                    }
                    break;
                }

                //set output objects param
                theReturn.nodes = theElms;

                //add functions to output object
                Object.keys(alsLibFunctions).forEach(function (thisFunction) {
                    theReturn[thisFunction] = alsLibFunctions[thisFunction];
                });

                //set length param
                theReturn.length = theElms.length;

                //has nodes
                theReturn.hasNodes = theReturn.length > 0
                    ? 1
                    : 0;
            } else {
                theReturn = false;
            }

            return theReturn;
        },
        returnBindEvent: function (thisNode, theCallback, theSelector) {
            return function (e) {
                var theNode = e.target;

                if (typeof document.addEventListener !== "function") {
                    theNode = window.event.srcElement;
                }

                thisNode.find(theSelector).each(function (returnNode) {
                    if (returnNode === theNode) {
                        theCallback.call(theNode, e);
                    } else if (returnNode === theNode.parentNode) {
                        theCallback.call(theNode.parentNode, e);
                    }
                });
            };
        }
    };

    alslib = function (theSelector, theNode) {
        return new alsLibCore.Init(theSelector, theNode);
    };

    window.alslib = alslib;
    window.alsLibFunctions = alsLibFunctions;

}());